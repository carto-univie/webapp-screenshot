# Webapp Screenshot Tool

This is a script to take screenshots of web applications. It is suitable for 
webapps that load additional data, such as images or XHR requests.

## Installation

This script requires a local installation of Firefox. Chrome will be installed 
automatically when downloading the puppeteer package.

Install the necessary NPM packages
```
$ npm install
```

To add suppoort for Firefox, install geckodriver https://github.com/mozilla/geckodriver
and make sure that it is executable and on your PATH.

## Running the script
```
$ node webapp-screenshot.js URL OUTPUT_FILE
```

There are a number of available command line options:
- `--browser=chrome` Set the brower, where ENGINE can be either "chrome" or "firefox"
- `--size=1280x960` Set the target image's dimensions
- `--deviceScaleFactor=1` Set the device scale factor (this is only available on chrome)
- `--type=png` Set the output file format to either "png" or "jpeg"
- `--element=body` The element to select for output