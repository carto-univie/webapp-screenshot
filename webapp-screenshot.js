#!/usr/bin/env node

const http = require('http');

const yargs = require('yargs');

const { writeFile } = require('fs');
const { promisify } = require('util');

const puppeteer = require('puppeteer');
const { Builder,promise } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');

const argv = yargs
    .usage('$0 <url> <output_file>', 'Take a screenshot of a webpage.', (yargs) => {
        yargs.positional('url', {
            describe: 'The URL of the webapp',
            type: 'string'
        });
        yargs.positional('output_file', {
            describe: 'Path to the output file',
            type: 'string'
        });
    })
    .option('browser', {
        alias: 'b',
        describe: 'Browser engine to use for rendering',
        choices: ['chrome', 'firefox']
    })
    .option('size', {
        alias: 's',
        describe: 'Dimension of the output image - e.g. 1280x1040'
    })
    .option('type', {
        alias: 't',
        describe: 'File type of the output image - png or jpeg',
        choices: ['png', 'jpeg']
    })
    .option('deviceScaleFactor', {
        alias: 'd',
        describe: 'Device scale factor - default 1',
    })
    .option('element', {
        alias: 'e',
        describe: 'The element to select - a valid DOM Element selector in CSS syntax. This only works for Chrome.'
    })
    .coerce('size', (arg) => {
        const matches = arg.match(/^(\d{1,4})[x,](\d{1,4})$/);

        if(!matches || matches.length != 3) {
            throw new Error('Invalid parameter for option "size"');
        }
        return {width: parseInt(matches[1]), height: parseInt(matches[2])};
    })
    .normalize('output_file')
    .default('browser', 'chrome')
    .default('size', '1024x768')
    .default('type', 'png')
    .default('deviceScaleFactor', 1)
    .default('element', 'body')
    .help()
    .argv;

const url = argv['url'];
const outputPath = argv['output_file'];
const dimensions = argv.size;
const scaleFactor = argv.deviceScaleFactor;
const filetype = argv.type;
const elementSelector = argv.element;

async function renderUsingChrome() {
    const getChromeInstance = () => {
        const chromeInfo = new Promise((resolve, reject) => {
            http.get('http://localhost:9222/json/version', (resp) => {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                });
                resp.on('end', () => {
                    resolve(JSON.parse(data));
                });
            }).on('error', () => {
                resolve(null);
            });
        });

        return chromeInfo
            .then((info) => {
                if(info) {
                    // connect to already running instance
                    const wsUrl = info.webSocketDebuggerUrl;
                    return puppeteer.connect({
                        browserWSEndpoint: wsUrl
                    }); 
                }
                // else launch new chrome instance
                return puppeteer.launch({
                    headless: true,
                    ignoreHTTPSErrors: true,
                    args: [
                        "--no-sandbox",
                        "--remote-debugging-port=9222"
                    ],
                    handleSIGTERM: false,
                    handeSIGHUP: false
                });
            });
    };

    // get a runnable chrome instance
    const browser = await getChromeInstance();

    const page = await browser.newPage();
    await page.setViewport({
        width: dimensions.width,
        height: dimensions.height,
        deviceScaleFactor: scaleFactor,
    });
    await page.goto(url, {waitUntil: "networkidle0"});
    await new Promise(resolve => setTimeout(resolve, 1000));
    const element = await page.$(elementSelector);
    if(element) {
        await element.screenshot({
            type: filetype,
            path: outputPath
        });
    } else {
        console.error('Element ' + elementSelector + ' cannot be found in DOM');
    }
    await page.close();

    if(browser.process()) {
        // was created with launch -> wait for all tabs to close before closing the browser
        let tabs = await browser.pages();
        while(tabs.length > 1) {
            await new Promise(resolve => setTimeout(resolve, 5000));
            tabs = await browser.pages();
        }
        await browser.close();
    } else {
        try {
            // was created with connect
            browser.disconnect();            
        } catch(e) {
            // ignore 
        }
    }
}

async function renderUsingFirefox() {
    // selenium promises conflict with async/await syntax - disable them
    promise.USE_PROMISE_MANAGER = false;

    const options = new firefox.Options();
    options.headless();
    options.windowSize({
        width: dimensions.width,
        height: parseInt(dimensions.height) + 74 // the width is off by 74px in Firefox ESR on Debian buster
    });

    const driver = new Builder()
        .forBrowser('firefox')
        .setFirefoxOptions(options)
        .build();

    try {
        await driver.get(url);
        await driver.wait(async () => {
            const readyState = await driver.executeScript('return document.readyState');
            return readyState === 'complete';
        });

        // Firefox does not support waiting for network idle.
        // We therefore wait for 10 seconds for image and XHR request 
        // to complete.
        await driver.sleep(10000);
        const data = await driver.takeScreenshot();
        await promisify(writeFile)(outputPath, data, 'base64');    
    } finally {
        await driver.quit();
    }
}

switch(argv.browser) {
    case 'firefox':
        renderUsingFirefox();
        break;
    case 'chrome':
    default:
        renderUsingChrome();
        break;
}
